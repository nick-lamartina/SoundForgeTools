

using System;
using System.Drawing;
using System.Collections;
using System.ComponentModel;
using System.Windows.Forms;
using SoundForge;

namespace Import_RX_Markers
{
    /// <summary>
    /// Summary description for MainForm.
    /// </summary>
    public class MainForm : System.Windows.Forms.Form
    {
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.RichTextBox RTTextBox;
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.Container components = null;

        /// <summary>
        /// Constructor for MainForm
        /// </summary>
        /// <param name="app"></param>
        public MainForm(SoundForge.IScriptableApp app)
        {
            //
            // Required for Windows Form Designer support
            //
            InitializeComponent();

            //
            // TODO: Add any constructor code after InitializeComponent call

            // enumerate the available effects and print out their names and GUIDS			

            int count = 1;
            this.RTTextBox.Text = "Enumerate Effects";
            this.RTTextBox.Text += Environment.NewLine + "There are " + app.Effects.Count + " effects available.";
            foreach (ISfGenericEffect fx in app.Effects)
            {
                this.RTTextBox.Text += Environment.NewLine + count + ": " + fx.Name;
                count++;
            }

            //an example of how to use DPF for debugging. 
            //Emits status text to the Script Editor
            EntryPoint.DPF("Total Effects: {0}", count);
        }

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (components != null)
                {
                    components.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code
        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTitle = new System.Windows.Forms.Label();
            this.RTTextBox = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // lblTitle
            // 
            this.lblTitle.Location = new System.Drawing.Point(40, 32);
            this.lblTitle.Name = "lblTitle";
            this.lblTitle.Size = new System.Drawing.Size(208, 40);
            this.lblTitle.TabIndex = 0;
            this.lblTitle.Text = "Sound Forge C# Test Rig";
            // 
            // RTTextBox
            // 
            this.RTTextBox.Location = new System.Drawing.Point(40, 80);
            this.RTTextBox.Name = "RTTextBox";
            this.RTTextBox.Size = new System.Drawing.Size(208, 160);
            this.RTTextBox.TabIndex = 1;
            this.RTTextBox.Text = "RTTextBox";
            // 
            // MainForm
            // 
            this.AutoScaleBaseSize = new System.Drawing.Size(5, 13);
            this.ClientSize = new System.Drawing.Size(292, 266);
            this.Controls.Add(this.RTTextBox);
            this.Controls.Add(this.lblTitle);
            this.Name = "MainForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "MainForm";
            this.ResumeLayout(false);

        }
        #endregion
    }
}
