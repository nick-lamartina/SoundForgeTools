/* =======================================================================================================
 *	Script Name: Insert Time-Labeled Marker
 *	Description: This script takes an RX-exported marker list and imports the positions into
 *	the open file, including their names (since RX Connect doesn't support markers)
 *
 *	Initial State: A file open
 *
 *	Output: Done with success or failure
 *
 * ==================================================================================================== */

using SoundForge;
using System;
using System.Windows.Forms;

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {
        ISfDataWnd wnd = app.ActiveWindow;

        // Make sure a file is open
        if (null == wnd)
            if (MessageBox.Show("Open a file before running this script.", "Nothing to process",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Open a file before running this script.";

        ISfFileHost file = app.CurrentFile;
        ISfPositionFormatter posFmt = file.Formatter;
        SfAudioMarkerList Markers = file.Markers;

        // Open the undo wrapper
        bool fCancel = false;
        int idUndo = file.BeginUndo(Script.Name);

        Markers.AddMarker(file.Window.Cursor, posFmt.PositionToTime(file.Window.Cursor).ToString());

        // Close the undo wrapper
        file.EndUndo(idUndo, fCancel);
        return null;
    }

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        Begin(app);
    }

    public static IScriptableApp ForgeApp = null;

    public static void DPF(string sz)
    {
        ForgeApp.OutputText(sz);
    }

    public static void DPF(string fmt, params object[] args)
    {
        ForgeApp.OutputText(String.Format(fmt, args));
    }
} //EntryPoint