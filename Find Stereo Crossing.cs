/* =======================================================================================================
 *	Script Name: Find Stereo Crossing
 *	Description: This script looks rightward for a sample where both channels cross at the same time.
 *
 *	Initial State: An open stereo file
 *
 *	Output: Done with success null status
 *
 * ==================================================================================================== */

using System;
using System.IO;
using System.Windows.Forms;
using SoundForge;

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {
        // NOTE: Make sure your "Zero-cross scan time" in Preferences -> Editing is set to 10,000
        // At the default of 1000, it's possible to fatally loop the script
        ISfDataWnd wnd = app.ActiveWindow;

        // Make sure a file is open
        if (null == wnd)
            if (MessageBox.Show("Open a file before running this script.", "Nothing to examine",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Open a file before running this script.";
        
        ISfFileHost file = app.CurrentFile;

        long zeroLeft = 0;  // Arbitrary value to start off with
        long zeroRight = 1;  // Offset by one so we don't trip the WHILE statement immediately
        long newLeft = file.Window.Cursor;  // Where we're going to start looking

        while (zeroLeft != zeroRight)
        {
            // Search rightward for the next left-side zero-crossing
            zeroLeft = file.SnapPositionToZeroCrossing(newLeft, 0, 1, 0);
            // Search for the closest right-side zero-crossing from the last-found left-side zero-crossing
            zeroRight = file.SnapPositionToZeroCrossing(zeroLeft, 0, 0, 1);
            newLeft = zeroLeft;
        }
        // Snap the cursor to the found sample
        file.Window.SetCursorAndScroll(zeroLeft, DataWndScrollTo.Nearest);
        return null;
    }

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        app.SetStatusText(String.Format("Script '{0}' is running.", Script.Name));
        string msg = Begin(app);
        app.SetStatusText(msg != null ? msg : String.Format("Double crossing found at sample {0}", app.CurrentFile.Window.Cursor));
    }
    public static IScriptableApp ForgeApp = null;
    public static void DPF(string sz) { ForgeApp.OutputText(sz); }
    public static void DPF(string fmt, object o) { ForgeApp.OutputText(String.Format(fmt, o)); }
    public static void DPF(string fmt, object o, object o2) { ForgeApp.OutputText(String.Format(fmt, o, o2)); }
    public static void DPF(string fmt, object o, object o2, object o3) { ForgeApp.OutputText(String.Format(fmt, o, o2, o3)); }
} //EntryPoint
