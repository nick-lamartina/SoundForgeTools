// BASIC C# Solution
// Demonstrates how to create a C# DLL that can be run from Sound Forge
// (scripting available in Sound Forge 8.0 or higher)
//
// Last Modified: 4/22/2005
// --------------------------------------------------------------------
//
// 
//Use Visual Studio for debugging:
// SETUP
// Assembly
// 1. Under asssembly ForgeScript, Properties, Configuration Properties, Debugging
//    - verify that the Debug Mode is Program
//    - Start Application = full path to Sound Forge EXE, including EXE
//    - Working Directory = path to Sound Forge EXE
// References 
// 1. Confirm that the path to Forge80.Script.dll is correct for your pc. If not, remove
//    it and re-add it.
//
// DEBUGGING
// 1. Rebuild your solution
// 2. Add breakpoints as desired
// 3. Start Debug (F5) to launch Sound Forge
// 4. From Sound Forge, select the menu Tools | Scripting, then Run Script
// 5. Navigate to the location of the ForgeSampleScript.dll and click Open
// 6. Visual Studio will run to your first breakpoint
// 7. Exit Sound Forge to finish debugging
//-------------------------------------------------------------


using System;
using System.Windows.Forms;
using SoundForge;

/// <summary>
/// Entry Point for Sound Forge
/// In order for Forge to run the C# DLL, the class EntryPoint MUST exist outside of the namespace
/// </summary>
public class EntryPoint
{
   /// <summary>
   /// Launches the primary class for this project & 
   /// </summary>
   /// <param name="app"></param>
   public void FromSoundForge(IScriptableApp app) 
   {
      ForgeApp = app; 
      app.SetStatusText(String.Format("Script '{0}' is running.", Script.Name));
      ForgeSampleScript.Engine.Begin(app); 
      app.SetStatusText(String.Format("Script '{0}' is done.", Script.Name));
   }
   
   public static IScriptableApp ForgeApp = null;
   
   //optional helper functions for output to the Script Editor
   //intended for debugging and development
   public static void DPF(string sz) { ForgeApp.OutputText(sz); }
   public static void DPF(string fmt, object o) { ForgeApp.OutputText(String.Format(fmt,o)); }
   public static void DPF(string fmt, object o, object o2) { ForgeApp.OutputText(String.Format(fmt,o,o2)); }
   
}
namespace ForgeSampleScript
{     
   public class Engine
   {
      private static MainForm form;
      
      /// <summary>
      /// Launches the GUI
      /// </summary>
      /// <param name="app"></param>
      public static void Begin(SoundForge.IScriptableApp app)
      {		         
      // Initialize the form & show it
      form = new MainForm(app);      								      
      form.ShowDialog();
      }
   }
}




