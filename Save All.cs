/* =======================================================================================================
 *	Script Name: Save All
 *	Description: This script saves every open file without a confirmation
 *
 *	Initial State: A file open
 *	Output: Done with success or failure
 *
 * ==================================================================================================== */

using System;
using System.IO;
using System.Windows.Forms;
using SoundForge;

//BEHAVIOR: Saves all open files without needing a confirmation from the user

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {
        ISfDataWnd wnd = app.ActiveWindow;
        // Make sure a file is open
        if (null == wnd)
            if (MessageBox.Show("Open a file before running this script.", "Nothing to save",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Open a file before running this script.";

        foreach (ISfFileHost openFile in app.Files)
        {
            openFile.Window.Active = true;
            openFile.Save(SaveOptions.WaitForDoneOrCancel);
            fileCount++;
            DPF("Saved {0}", openFile.Filename);
        } // foreach open file
        return null;
    }

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        app.SetStatusText(String.Format("Script '{0}' is running.", Script.Name));
        string msg = Begin(app);
        string fStr = "files";
        if (fileCount == 1) { fStr = "file"; }

        app.SetStatusText(msg != null ? msg : String.Format("Saved {0} {1}.", fileCount, fStr));
    }
    public static IScriptableApp ForgeApp = null;
    public static void DPF(string sz) { ForgeApp.OutputText(sz); }
    public static void DPF(string fmt, object o) { ForgeApp.OutputText(String.Format(fmt, o)); }
    public static void DPF(string fmt, object o, object o2) { ForgeApp.OutputText(String.Format(fmt, o, o2)); }
    public static void DPF(string fmt, object o, object o2, object o3) { ForgeApp.OutputText(String.Format(fmt, o, o2, o3)); }
    public Int32 fileCount = 0;
} //EntryPoint
