/* =======================================================================================================
 *	Script Name: Open File Without Drawing
 *	Description: This script opens a file without drawing peaks. Meant to be used on a command line
 *	when using Sound Forge to process/read files when manual manipulation is not necessary. Note that
 *	if you want to run another script while these non-drawn files are open, the Window.Active method
 *	of flipping through files will fail the process. Don't bother with this step and everything will
 *	work as intended.
 *
 *	Initial State: None
 *
 *	Output: None
 *
 * ==================================================================================================== */

using System;
using System.Windows.Forms;
using SoundForge;

public class EntryPoint
{
    public void Begin(IScriptableApp app)
    {
        string sFile = GETARG("Open", @"");  // This is a required argument, or else the script won't do anything
        // Open the file without drawing peaks
        ISfFileHost file = app.OpenFile(sFile, false, true);
    }

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        Begin(app);
    }
    public static IScriptableApp ForgeApp = null;
    public static void DPF(string sz) { ForgeApp.OutputText(sz); }
    public static void DPF(string fmt, params object[] args) { ForgeApp.OutputText(String.Format(fmt, args)); }
    public static string GETARG(string k, string d) { string val = Script.Args.ValueOf(k); if (val == null || val.Length == 0) val = d; return val; }
    public static int GETARG(string k, int d) { string s = Script.Args.ValueOf(k); if (s == null || s.Length == 0) return d; else return Script.Args.AsInt(k); }
    public static bool GETARG(string k, bool d) { string s = Script.Args.ValueOf(k); if (s == null || s.Length == 0) return d; else return Script.Args.AsBool(k); }
    public static double GETARG(string k, double d)
    {
        string s = Script.Args.ValueOf(k);
        if (s == null || s.Length == 0)
            return d;
        else
            try { d = double.Parse(s); }
            catch { }
        return d;
    }
} //EntryPoint
