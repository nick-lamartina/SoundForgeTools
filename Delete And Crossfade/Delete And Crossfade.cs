/* =======================================================================================================
 *	Script Name: Delete And Crossfade
 *	Description: This script deletes the seleted area and crossfades the edges of the remaining audio.
 *
 *	Initial State: A file open with a selection
 *
 *	Output: Done with success or failure
 *
 * ==================================================================================================== */

using System;
using System.Windows.Forms;
using SoundForge;

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {
        ISfDataWnd wnd = app.ActiveWindow;
        if (null == wnd)
            return "Open a file before running this script.";

        ISfFileHost file = wnd.File;

        string sFadeSize = GETARG("FadeSize", @"0.5");
        double dFadeSize = Convert.ToDouble(sFadeSize);

        Int64 ccFadeLen = file.SecondsToPosition(dFadeSize);
        Int64 ccFadeLeftEnd = wnd.Selection.ccStart;
        Int64 ccFadeRightStart = wnd.Selection.ccStart + wnd.Selection.ccLength;
        Int64 ccFadeLeftStart = ccFadeLeftEnd - ccFadeLen;
        Int64 ccFadeRightEnd = ccFadeRightStart + ccFadeLen;

        SfAudioCrossfade cfFade = new SfAudioCrossfade(ccFadeLen);

        // Open the undo wrapper
        bool fCancel = false;
        int idUndo = file.BeginUndo(Script.Name);

        SfAudioSelection selDelete = new SfAudioSelection(ccFadeLeftStart, wnd.Selection.ccLength + ccFadeLen);
        SfAudioSelection selLeftFade = new SfAudioSelection(ccFadeLeftStart, ccFadeLen);
        SfAudioSelection selRightFade = new SfAudioSelection(ccFadeRightStart, ccFadeLen);

        file.DoMixReplace(selRightFade, 0, 1, file, selLeftFade, null, cfFade, EffectOptions.EffectOnly);
        file.DeleteAudio(selDelete);

        // Close the undo wrapper
        file.EndUndo(idUndo, fCancel);

        return null;
    }

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        app.SetStatusText(String.Format("Script '{0}' is running.", Script.Name));
        Begin(app);
        app.SetStatusText(String.Format("Script '{0}' is done.", Script.Name));
    }
    public static IScriptableApp ForgeApp = null;
    public static void DPF(string sz) { ForgeApp.OutputText(sz); }
    public static void DPF(string fmt, params object[] args) { ForgeApp.OutputText(String.Format(fmt, args)); }
    public static string GETARG(string k, string d) { string val = Script.Args.ValueOf(k); if (val == null || val.Length == 0) val = d; return val; }
    public static int GETARG(string k, int d) { string s = Script.Args.ValueOf(k); if (s == null || s.Length == 0) return d; else return Script.Args.AsInt(k); }
    public static bool GETARG(string k, bool d) { string s = Script.Args.ValueOf(k); if (s == null || s.Length == 0) return d; else return Script.Args.AsBool(k); }
} //EntryPoint