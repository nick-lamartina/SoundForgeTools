/* =======================================================================================================
 *  Script Name: iZotope Alloy 2
 *  Description: This script runs iZotope Alooy 5.
 *  Author: Nick LaMartina (nick.lamartina+audio@gmail.com)
 *  Last Edited: 2015-02-24
 *
 *  Initial State: An open file
 *  Output: Done with success or failure
 *
 * ==================================================================================================== */

using System;
using System.IO;
using System.Windows.Forms;
using SoundForge;

//BEHAVIOR: Takes an open file runs iZotope Alloy 2

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {

        ISfFileHost file = app.CurrentFile;
        ISfDataWnd wnd = app.ActiveWindow;

        // Make sure a file is open
        if (null == wnd)
            if (MessageBox.Show("Open a file before running this script.", "Nothing to process",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Open a file before running this script.";

        // Open the effect
        try
        {
            file.DoEffect("iZotope Alloy 2", "", new SfAudioSelection(wnd), EffectOptions.DialogFirst | EffectOptions.WaitForDoneOrCancel);
        }
        catch (Exception)
        {
            return "iZotope Alloy 2 did not complete, quitting script";
        }
        return null;
    }

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        app.SetStatusText(String.Format("iZotope Alloy 2 is running.", Script.Name));
        string msg = Begin(app);
        app.SetStatusText(msg != null ? msg : String.Format("iZotope Alloy 2 is done.", Script.Name));
    }
    public static IScriptableApp ForgeApp = null;
} //EntryPoint
