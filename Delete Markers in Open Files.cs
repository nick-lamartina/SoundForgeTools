/* =======================================================================================================
 *	Script Name: Delete Markers in Open Files
 *	Description: This script removes all markers (markers, regions, loops, commands, etc) from all open files.
 *
 *	Initial State: A file open with markers
 *
 *	Output: Done with success or failure
 *
 * ==================================================================================================== */

using System;
using System.IO;
using System.Windows.Forms;
using SoundForge;

//Run with a file open that contains markers
//Uses an undo wrapper, so that you can redo all markers in one step

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {
        ISfDataWnd wnd = app.ActiveWindow;

        // Make sure a file is open
        if (null == wnd)
            if (MessageBox.Show("Open a file before running this script.", "Nothing to process",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Open a file before running this script.";

        foreach (ISfFileHost openFile in app.Files)
        {
            openFile.Window.Active = true;
            ISfFileHost file = app.CurrentFile;
            SfAudioMarkerList Markers = file.Markers;
            int cMarkers = Markers.Count;

            fileCount++;

            if (cMarkers <= 0 && file.Sampler.LoopCount <= 0)
            {
                continue; // Go to the next file is there's nothing to do
            }
            // create undo wrapper: just one undo for whole operation
            bool fCancel = false;
            int idUndo = file.BeginUndo("Remove Markers");

            markerCount += file.Sampler.LoopCount;
            markerCount += cMarkers;

            file.Sampler.LoopCount = 0;  // Remove sample loops
            file.Markers.Clear();  // Remove markers

            // close undo wrapper and keep changes if we removed any markers.
            file.EndUndo(idUndo, fCancel);

            DPF("Removed markers from {0}", file.Filename);
        } // foreach
        return null;
    } // Begin

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        app.SetStatusText(String.Format("Script '{0}' is running.", Script.Name));
        string msg = Begin(app);
        string mStr = "markers";
        string fStr = "files";
        if (markerCount == 1) { mStr = "marker"; }
        if (fileCount == 1) { fStr = "file"; }

        app.SetStatusText(msg != null ? msg : String.Format("Removed {0} {2} from {1} {3}.", markerCount, fileCount, mStr, fStr));
    }
    public static IScriptableApp ForgeApp = null;
    public static void DPF(string sz) { ForgeApp.OutputText(sz); }
    public static void DPF(string fmt, object o) { ForgeApp.OutputText(String.Format(fmt, o)); }
    public static void DPF(string fmt, object o, object o2) { ForgeApp.OutputText(String.Format(fmt, o, o2)); }
    public static void DPF(string fmt, object o, object o2, object o3) { ForgeApp.OutputText(String.Format(fmt, o, o2, o3)); }
    public Int32 fileCount = 0;
    public Int64 markerCount = 0;

} //EntryPoint