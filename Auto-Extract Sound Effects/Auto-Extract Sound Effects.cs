/* =======================================================================================================
 *	Script Name: Auto-Extract Sound Effects
 *	Description: This script extracts individual sound effects from a linear render
 *
 *	Initial State: A file open without markers and silences in-between rendered effects
 *
 *	Output: Done with success or failure
 *
 * ==================================================================================================== */

using System;
using System.IO;
using System.Windows.Forms;
using SoundForge;

// Run with a file open that doesn't contain markers
// Extracts files using a naming convention

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {
        ISfDataWnd wnd = app.ActiveWindow;

        // Make sure a file is open
        if (null == wnd)
            if (MessageBox.Show("Open a file before running this script.", "Nothing to process",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Open a file before running this script.";

        ISfFileHost file = app.CurrentFile;
        
        ISfGenericEffect fx = app.FindEffect("Auto Trim/Crop");

        byte[] abData1 = new byte[28] { 2, 0, 0, 0, 124, 243, 255, 255, 184, 242, 255, 255, 20, 0, 0, 0, 20, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, };
        byte[] abData2 = new byte[28] { 2, 0, 0, 0, 124, 243, 255, 255, 184, 242, 255, 255, 20, 0, 0, 0, 20, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, };

        // Make the presets
        ISfGenericPreset preMakeRegions = new SoundForge.SfGenericPreset("Make Regions", fx, abData1);
        ISfGenericPreset preTrimEffects = new SoundForge.SfGenericPreset("Trim Effects", fx, abData2);

        // Create objects that will let us set fields in the presets
        Fields_AutoTrimCrop fields1 = Fields_AutoTrimCrop.FromPreset(preMakeRegions);
        Fields_AutoTrimCrop fields2 = Fields_AutoTrimCrop.FromPreset(preTrimEffects);

        // Set values for the relevant fields
        fields1.InTime = 0;
        fields1.OutTime = 0;
        fields1.MinSilence = 0.1;
        fields1.StartThreshold = SfHelpers.dBToRatio(-79.99);
        fields1.EndThreshold = SfHelpers.dBToRatio(-79.99);
        fields1.Function = Fields_AutoTrimCrop.CropFunction.RemoveSilence;

        fields2.InTime = 0;
        fields2.OutTime = 0;
        fields2.StartThreshold = SfHelpers.dBToRatio(-79.99);
        fields2.EndThreshold = SfHelpers.dBToRatio(-79.99);
        fields2.Function = Fields_AutoTrimCrop.CropFunction.RemoveEdges;

        // Send the field data to the presets
        fields1.ToPreset(preMakeRegions);
        fields2.ToPreset(preTrimEffects);

        // Use the first preset to establish regions around each effect
        app.DoEffect("Process.AutoTrimCrop", preMakeRegions, EffectOptions.EffectOnly);

        // Refresh the file stats so that the marker list gets updated
        file.UpdateStatistics(new SfAudioSelection(0, -1));

        string szDir = Path.GetDirectoryName(file.Filename);
        string szExt = Path.GetExtension(file.Filename);
        szParentFile = wnd.Title;

        // Process each region
        foreach (SfAudioMarker mk in file.Markers)
        {
            if (mk.Length <= 0)
                continue;

            // Create a new file for each region
            ISfFileHost fileT = file.NewFile(new SfAudioSelection(mk.Start, mk.Length));
            if (null == fileT)
                break;

            // Make a new new based off the parent file
            string szName = String.Format("{0}_{1}_{2}{3}", wnd.Title.Replace(".wav", ""), "extract", mk.Ident, szExt);
            szName = SfHelpers.CleanForFilename(szName);

            // Save the file and open it
            fileT.SaveAs(Path.Combine(szDir, szName), file.SaveFormat.Guid, 0, RenderOptions.RenderOnly | RenderOptions.Reopen);
            SfStatus result = fileT.WaitForDoneOrCancel();
            if (result != SfStatus.Success)
            {
                DPF("SaveAs failed - {0} quitting script", result);
                break;
            }

            // Use the second preset to trim away the silent areas
            fileT.DoEffect("Process.AutoTrimCrop", preTrimEffects, new SfAudioSelection(0, -1), EffectOptions.EffectOnly);
            // Wait, just in case things haven't caught up yet
            fileT.WaitForDoneOrCancel();
            // Clear the markers, then save
            fileT.Markers.Clear();
            fileT.Save(SaveOptions.WaitForDoneOrCancel);
            iFileCount++;
        }// foreach

        return null;
    }

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        app.SetStatusText(String.Format("Script '{0}' is running.", Script.Name));
        Begin(app);
        string fStr = "files";
        if (iFileCount == 1) { fStr = "file"; }
        app.SetStatusText(String.Format("Extracted {0} {1} from '{2}'.", iFileCount, fStr, szParentFile));
    }
    public static IScriptableApp ForgeApp = null;
    public static void DPF(string sz) { ForgeApp.OutputText(sz); }
    public static void DPF(string fmt, params object[] args) { ForgeApp.OutputText(String.Format(fmt, args)); }
    public Int32 iFileCount = 0;
    public string szParentFile = "parent file";
} //EntryPoint
