/* =======================================================================================================
 *	Script: Cross-fade release into sustain
 *	
 *	Description: This script takes a selected area in an open file, crossfades the release into the
 *	start of the sustain section, and discards the tail.
 *
 *	Initial State: A file open with a selection
 *
 *	Output: Done with success or failure
 *
 * ==================================================================================================== */

using System;
using System.IO;
using System.Windows.Forms;
using SoundForge;

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {
        ISfDataWnd wnd = app.ActiveWindow;

        // Make sure a file is open
        if (null == wnd)
            if (MessageBox.Show("Open a file before running this script.", "Nothing to process",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Open a file before running this script.";
        // Make sure an edit region is highlighted
        if (wnd.SelectionLength == 0)
        {
            if (MessageBox.Show("Highlight a selection length before running this script.", "No selection",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Highlight a selection length before running this script.";
        }

        ISfFileHost file = wnd.File;

        Int64 ccLoopStart = wnd.Selection.ccStart;
        Int64 ccTailStart = wnd.Selection.ccStart + wnd.Selection.Length;
        Int64 ccTailEnd = wnd.File.Length;

        DateTime timeStamp = DateTime.Now;
        string mkDone = "Looped on " + timeStamp.ToString();

        // Check for any conditions that will cause the script to fail
        if (ccLoopStart == 0 || ccTailStart == ccTailEnd)
            if (MessageBox.Show("The edges of the sustain cannot be either the beginning or end of the file.", "Selection cannot be cross-faded",
                               MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                return "The edges of the sustain cannot be either the beginning or end of the file.";

        if (ccTailEnd - ccTailStart + ccLoopStart >= ccTailStart - ccLoopStart)
            if (MessageBox.Show("The combined attack and release cannot be longer than the sustain.", "Can't fit loop",
                               MessageBoxButtons.OK, MessageBoxIcon.Error) == DialogResult.OK)
                return "The combined attack and release cannot be longer than the sustain.";

        // Open the undo wrapper
        bool fCancel = false;
        int idUndo = file.BeginUndo(Script.Name);

        // Deliniate the head and tail sections
        SfAudioSelection selLoopHead = new SfAudioSelection(0, ccLoopStart + (ccTailEnd - ccTailStart));
        SfAudioSelection selLoopTail = new SfAudioSelection(ccTailStart - ccLoopStart, ccLoopStart + (ccTailEnd - ccTailStart));

        // Perform the fades
        file.DoEffect("Graphic Fade", "-3 dB exponential fade in", selLoopHead, EffectOptions.EffectOnly);
        file.DoEffect("Graphic Fade", "-3 dB exponential fade out", selLoopTail, EffectOptions.EffectOnly);
        // Select the head and tail again not that the fade has been processed
        selLoopHead = new SfAudioSelection(0, ccLoopStart + (ccTailEnd - ccTailStart));
        selLoopTail = new SfAudioSelection(ccTailStart - ccLoopStart, ccLoopStart + (ccTailEnd - ccTailStart));
        // Mix the tail into the head
        file.MixAudio(selLoopHead, 1, 1, file, selLoopTail);
        // Remove the tail, now that it's trash
        file.DeleteAudio(selLoopTail);

        // Set the loop region to the entire file for quick auditioning
        wnd.Selection = new SfAudioSelection(0, file.Length);
        wnd.SelectionLength = 0;

        // Time stamp the loop with a marker so we know if it's been processed or not when checking later on.
        SfAudioMarkerList Markers = file.Markers;
        SfAudioMarker mk = new SfAudioMarker(0);
        mk.Name = mkDone;
        Markers.Add(mk);

        // Close the undo wrapper
        file.EndUndo(idUndo, fCancel);
        DPF("Done - {0}", fCancel ? "failed and rewound changes" : "success");
        if (fCancel)
            return "Script cancelled.";
        else
            return null;
    } //Begin

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        app.SetStatusText(String.Format("Script '{0}' is running.", Script.Name));
        string msg = Begin(app);
        app.SetStatusText(msg != null ? msg : String.Format("Script '{0}' is done.", Script.Name));
    }
    public static IScriptableApp ForgeApp = null;
    public static void DPF(string sz) { ForgeApp.OutputText(sz); }
    public static void DPF(string fmt, params object[] args) { ForgeApp.OutputText(String.Format(fmt, args)); }
} //EntryPoint