/* =======================================================================================================
 *	Script Name: Import RX Markers
 *	Description: This script takes an RX-exported marker list and imports the positions into
 *	the open file, including their names (since RX Connect doesn't support markers)
 *	
 *	Initial State: A file open
 *
 *	Output: Done with success or failure
 *
 * ==================================================================================================== */

using System;
using System.IO;
using System.Windows.Forms;
using SoundForge;

public class EntryPoint
{
    public string Begin(IScriptableApp app)
    {
        ISfDataWnd wnd = app.ActiveWindow;

        // Make sure a file is open
        if (null == wnd)
            if (MessageBox.Show("Open a file before running this script.", "Nothing to process",
                               MessageBoxButtons.OK, MessageBoxIcon.Warning) == DialogResult.OK)
                return "Open a file before running this script.";

        // Set up the file selection dialog
        OpenFileDialog fileDialog = new OpenFileDialog();
        fileDialog.Title = "Open the RX-Exported Text File";
        fileDialog.Filter = "TXT file|*.txt";
        fileDialog.InitialDirectory = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        string strFileName = "";
        // Now open the dialog
        if (fileDialog.ShowDialog() == DialogResult.OK)
            strFileName = fileDialog.FileName;
        if (strFileName == String.Empty)
            return "You must open a marker list before continuing.";

        ISfFileHost file = app.CurrentFile;
        ISfPositionFormatter posFmt = file.Formatter;
        string line;
        long fileLength = file.Length;

        // Open the undo wrapper
        bool fCancel = false;
        int idUndo = file.BeginUndo(Script.Name);
        // Open the source file
        using (StreamReader textFile = new StreamReader(strFileName))
        {
            while ((line = textFile.ReadLine()) != null)
            {
                // Delimit the tab-based file that RX has exported
                char[] delimiters = new char[] { '\t' };
                string[] parts = line.Split(delimiters, StringSplitOptions.None);
                // If the line is longer than two sections and has only one time, it's a marker
                if (parts.Length >= 2 && parts[2] == "")
                {
                    // Converting from time to seconds to samples since ParsePosition
                    // doesn't want to behave as intended.
                    double ccSec = posFmt.ParseTime(parts[1]);
                    long ccSamp = posFmt.TimeToPosition(ccSec);
                    if (ccSamp > fileLength)
                    {
                        file.EndUndo(idUndo, true);
                        if (MessageBox.Show("The open audio file is too short for the marker list being imported. " +
                            "Are you sure you're importing the right one?",
                            "Import doesn't match file length",
                            MessageBoxButtons.OK, MessageBoxIcon.Question) == DialogResult.OK)
                            return "This file is too short for the marker list. Double-check your import.";
                    }
                    SfAudioMarkerList Markers = file.Markers;
                    SfAudioMarker mk = new SfAudioMarker(ccSamp);
                    mk.Name = parts[0];
                    Markers.Add(mk);
                    markCount++;
                }
                // If the line is longer than two sections and has two times, it's a region
                else if (parts.Length >= 3)
                {
                    // Converting from time to seconds to samples since ParsePosition
                    // doesn't want to behave as intended.
                    double ccSec1 = posFmt.ParseTime(parts[1]);
                    double ccSec2 = posFmt.ParseTime(parts[2]);
                    long ccSamp1 = posFmt.TimeToPosition(ccSec1);
                    long ccSamp2 = posFmt.TimeToPosition(ccSec2) - ccSamp1;
                    if (ccSamp1 + ccSamp2 > fileLength)
                    {
                        file.EndUndo(idUndo, true);
                        if (MessageBox.Show("The open audio file is too short for the marker list being imported. " +
                            "Are you sure you're importing the right one?",
                            "Import doesn't match file length",
                            MessageBoxButtons.OK, MessageBoxIcon.Question) == DialogResult.OK)
                            return "This file is too short for the marker list. Double-check your import.";
                    }
                    SfAudioMarkerList Markers = file.Markers;
                    SfAudioMarker rg = new SfAudioMarker(ccSamp1, ccSamp2);
                    rg.Name = parts[0];
                    Markers.Add(rg);
                    regCount++;
                }
            }//while
            textFile.Close();
        }//using StreamReady

        // Close the undo wrapper
        file.EndUndo(idUndo, fCancel);
        return null;
    }

    public void FromSoundForge(IScriptableApp app)
    {
        ForgeApp = app; //execution begins here
        app.SetStatusText(String.Format("Script '{0}' is running.", Script.Name));
        string msg = Begin(app);
        string mStr = "markers";
        string rStr = "regions";
        if (markCount == 1) { mStr = "marker"; }
        if (regCount == 1) { rStr = "marker"; }
        app.SetStatusText(msg != null ? msg : String.Format("Imported {0} {2} and {1} {3}.", markCount, regCount, mStr, rStr));
    }
    public Int64 markCount = 0;
    public Int64 regCount = 0;
    public static IScriptableApp ForgeApp = null;
    public static void DPF(string sz) { ForgeApp.OutputText(sz); }
    public static void DPF(string fmt, params object[] args) { ForgeApp.OutputText(String.Format(fmt, args)); }
} //EntryPoint
